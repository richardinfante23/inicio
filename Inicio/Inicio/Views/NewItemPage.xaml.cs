﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Inicio.Models;
using Inicio.ViewModels;

namespace Inicio.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewItemPage : ContentPage
    {
        public Result Item { get; set; }



        public NewItemPage(ItemsViewModel viewModel)
        {
            InitializeComponent();

            Item = new Result
            {
                FirstName = "Nombre",
                LastName = "Apellido",
                Phone = "9999-9999-999",
                Links = new Links
                {

                    Avatar = new Avatar
                    {
                        Href = new Uri("https://lorempixel.com/250/250/people/?93182", UriKind.Absolute)
                    }
                }
            };
    
           viewModel.usuario = new Result();
            BindingContext = viewModel;
        
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
       
            await Navigation.PopModalAsync();
        }
    }
}