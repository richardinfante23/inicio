﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using Inicio.Models;
using Inicio.Views;

namespace Inicio.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<Result> Items { get; set; }
        public Command LoadItemsCommand { get; set; }
        public Command GuardarCommand { get; set; }

        public ItemsViewModel()
        {
            Title = "Browse";
            Items = new ObservableCollection<Result>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            GuardarCommand= new Command(  () =>
            {
           
                Items.Add(usuario);
                usuario = new Result();
            });

        
        }

       public Result usuario
        {
            get;set;
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}