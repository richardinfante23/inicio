﻿using Inicio.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Inicio.Services
{
   


    public class MockDataStoreUser : IDataStore<Result>
    {
        Result[] UsuarioResult;
        Objeto usuario = new Objeto();
        List<Usuarios> items;
        HttpClient _cliente = new HttpClient();
        string UrlCliente = "https://gorest.co.in/public-api/users";
        public async Task MockDataStoreUserAsync()
        {

            UsuarioResult = await listausuarios();


        }

        public async Task<bool> AddItemAsync(Result item)
        {


            UsuarioResult = await listausuarios();

            UsuarioResult[UsuarioResult.Count()]=item;

            return await Task.FromResult(true);
        }

        public  async Task<Result[]> listausuarios() {
      
        
            try { 
            _cliente.DefaultRequestHeaders.Clear();
            _cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", "HOEW7pqY-ySUIGFkrkEs3OI6gm9Wyy8gPeH6");
         
            var response = await _cliente.GetAsync(UrlCliente);
                response.Headers.Clear();
                var conten=   response.Content.ReadAsStringAsync().Result;
                    usuario= JsonConvert.DeserializeObject<Objeto>(conten); 

        }
            catch (Exception ex)
            {

                throw new Exception(ex.Message, ex.InnerException);
       }
        
            return usuario.Result;
        }

        public async Task<bool> UpdateItemAsync(Usuarios item)
        {
            //var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            //items.Remove(oldItem);
            //items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            //var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            //items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Result> GetItemAsync(string id)
        {
        
            return await Task.FromResult(usuario.Result.FirstOrDefault(s => s.FirstName == id));
        }

        public async Task<IEnumerable<Result>> GetItemsAsync(bool forceRefresh = false)
        {
            UsuarioResult = await listausuarios();

            return await Task.FromResult(UsuarioResult);
        }

     

        public Task<bool> UpdateItemAsync(Result item)
        {
            throw new NotImplementedException();
        }

    
      
    }

}
